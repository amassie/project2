#include "ci_tcp.h"
#include "ci_packet.h"
#include <string.h>

// use this for debugging purpsoes.
void print_packet(ci_packet_t *pkt) 
{
    printf("ident: 0x%x" PRIu16 "\n",pkt->hdr.identifier);
    printf("src port: %" PRIu16 "\n", pkt->hdr.src_port);
    printf("dst port: %" PRIu16 "\n", pkt->hdr.dst_port);
    printf("seq: %" PRIu32 "\n", pkt->hdr.seq);
    printf("ack: %" PRIu32 "\n", pkt->hdr.ack);
    printf("hlen: %" PRIu16 "\n",pkt->hdr.hlen);
    printf("plen: %" PRIu16 "\n",pkt->hdr.plen);
    printf("flag: %u\n", pkt->hdr.flags);
    printf("window: %" PRIu16 "\n",pkt->hdr.window);
    printf("data: %s\n", pkt->data);
}

ci_packet_t *make_packet(ci_conn_t *con, char *data, int len, uint32_t seq, uint32_t ack, 
                            uint16_t window, uint8_t flags) 
{
    ci_packet_t *pkt = malloc(sizeof(ci_packet_t));

    pkt->hdr.identifier = (uint16_t) TEAM_IDENTIFIER;
    pkt->hdr.src_port = con->src_port;
    pkt->hdr.dst_port = con->dst_port;
    pkt->hdr.seq = seq;
    pkt->hdr.ack = ack;
    pkt->hdr.hlen = HEADER_LEN;
    pkt->hdr.plen = HEADER_LEN + len;
    pkt->hdr.flags = flags;
    pkt->hdr.window = window;

    if(len > 0) 
	{
        strcpy(pkt->data, data);
    }

    return pkt;
}


void resend_data(ci_conn_t *con, char *data, int len)
{
	ci_packet_t *pkt;
	pkt = make_packet(con, data, len, con->last_ack, con->last_ack, 0, 0);
	// send the packet itself.
	sendto(con->sfd, pkt, pkt->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);
}



//I give up I can't come up with a clever name for this one
void window_timer(ci_conn_t *con){
	int index;
	for (index=0; index< WINDOW_SIZE;index++){
		if(con->slidingWindow[index].timeWaiting==-1){
			//returnValue=1;
		}
		else{
			con->slidingWindow[index].timeWaiting++;
		}
	}
}


int window_peeper(ci_conn_t *con){
	int returnValue=0;
	int index;
	for (index=0; index< WINDOW_SIZE;index++){
		if(con->slidingWindow[index].timeWaiting>=WINDOW_WAIT_TIME){

			resend_data(con, &(con->slidingWindow[index].packet.data), strlen(con->slidingWindow[index].packet.data));
			con->slidingWindow[index].timeWaiting=0;
		}

		//check if there is a free space
		if(con->slidingWindow[index].timeWaiting==-1){
			returnValue=1;
		}

	}

	return returnValue;
	//check if sliding window is full
	//check if a packet has been waiting too long
}

//add to first open slot in sliding window
int window_smudger(ci_conn_t *con, ci_packet_t* data){
	int result=-1;

	int index;
	for (index=0; index< WINDOW_SIZE;index++){
		if(con->slidingWindow[index].timeWaiting==-1){
			memcpy(con->slidingWindow[index].packet.data,data, sizeof(ci_packet_t));
			con->slidingWindow[index].timeWaiting=0;
			result=0;
		}
	}
	return result;
}

//check ack and remove from sliding window if a match is found
void window_cleaner(ci_conn_t *con, uint32_t ack ){
	//check through packets for ack number
	//if it finds it then it removes it
	int index;
	for (index=0; index< WINDOW_SIZE;index++)
	{
		if ((con->slidingWindow[index].packet.hdr.ack) == ack)
		{
			con->slidingWindow[index].timeWaiting = -1;
			printf("\nWINDOW CLEANED!");
		}
	}
}




void send_data(ci_conn_t *con, char *data, int len) 
{
    ci_packet_t *pkt;
    while(len > 0) 
	{
        pkt = make_packet(con, data, len, con->last_ack, con->last_ack, 0, 0);
        // send the packet itself.
        sendto(con->sfd, pkt, pkt->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);

        len -= len;//I don't understand Kevin... why?
    }

	window_smudger(con, pkt);
}

void send_flag(ci_conn_t *con, int flag)
{
	ci_packet_t *pkt;

	pkt = make_packet(con, "", HEADER_LEN, con->last_ack, con->last_ack, 0, flag);
	// send the packet itself.
	sendto(con->sfd, pkt, pkt->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);

	//TODO DEAL WITH ACK
	recv_data(con, 0, 1); // hoping to get an ack, timeout otherwise
}

int recv_data(ci_conn_t *con, int wait, int timeout) 
{
	int return_flag = -1;
    int buff_len = MAX_DATA + HEADER_LEN + C_PADDING;
    char buffer[buff_len];
    // time out handler using select()
    // See UNIX Networking Book for details Chapter 14 section 2
    fd_set afd; // timeout if no acknowledgement
    struct timeval timeout_time;
    timeout_time.tv_sec = TIMEOUT_SEC_DEFAULT;
    timeout_time.tv_usec = TIMEOUT_MICROSEC_DEFAULT;

    pthread_mutex_lock(&(con->recv_lock));

    // just take a peeksy of the recvfrom buffer
    if(wait) 
	{
        buff_len = recvfrom(con->sfd, buffer, buff_len, MSG_PEEK, (struct sockaddr *)&con->client_addr, &con->client_addr_len);
    } 
	else if(timeout) 
	{
        // wait for something read, give up after a poorly selected default time
        FD_ZERO(&afd);
        FD_SET(con->sfd, &afd);
        select(con->sfd + 1, &afd, NULL, NULL, &timeout_time);
    } 
	else 
	{
        buff_len = recvfrom(con->sfd, buffer, buff_len, MSG_DONTWAIT | MSG_PEEK, (struct sockaddr *)&con->client_addr, &con->client_addr_len);
    }
		
    // oh my, there is data, let's get it.
    if(buff_len > 0) 
	{
        ci_packet_t *pkt = malloc(sizeof(ci_packet_t));
        int read = 0;
        // get it all out from the boofer
        while(read < buff_len) 
		{
            read = recvfrom(con->sfd, pkt + read, buff_len - read, 0, (struct sockaddr *)&con->client_addr, &con->client_addr_len);
            read += buff_len;
        }
        con->recv_buffer_len = read;
        con->recv_buffer = malloc(read - HEADER_LEN - C_PADDING);
        strcpy(con->recv_buffer, pkt->data);

		return_flag = pkt->hdr.flags;
		
        // helps with debugging.
        //print_packet(pkt);
		
        // handle the packet based on what we got
        // if it's an ACK, see if we should acknowledge it
        // anything else, send an acknoledgement!
        if(pkt->hdr.flags == ACK) 
		{
            //todo: this should probably check to make sure the packet numbers match

            // update acknowledgement
            pthread_mutex_lock(&(con->ack_lock));
            if(pkt->hdr.ack > con->last_ack) 
			{
                con->last_ack = pkt->hdr.ack;
            }


			window_cleaner(con,pkt->hdr.ack-1);

            pthread_mutex_unlock(&(con->ack_lock));
        }		
		else if(pkt->hdr.flags == SYN) 
		{			
			printf("Send SYNACK\n");
            // send an SYNACK
            ci_packet_t *reply;
            reply = make_packet(con, "", HEADER_LEN, 0, 0, 0, SYNACK);
            sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, (struct sockaddr *)&con->client_addr, con->client_addr_len);
        }
        else
        {
			printf("Send ACK\n");
            // send an acknowledgement
            ci_packet_t *reply;
            reply = make_packet(con, "", HEADER_LEN, pkt->hdr.seq, pkt->hdr.seq + 1, 0, ACK);
            sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, (struct sockaddr *)&con->client_addr, con->client_addr_len);

            con->last_seq = pkt->hdr.seq;
        }
    }
    pthread_mutex_unlock(&(con->recv_lock));

	return return_flag;
}

void *main_loop(void *con_info) 
{
    ci_conn_t *con = (ci_conn_t *) con_info;
    int closing;

    char *send_buffer;
    int send_len;
	int waiting_queue=0;
	printf("STARTING!!!\n");
	
	while(con->state != STATE_FIN_DONE)
	{
		if(con->state == STATE_CLOSED)
		{
			ci_packet_t *reply;
			reply = make_packet(con, "", HEADER_LEN, 0, 0, 0, SYN);
			sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);
			//con->state == STATE_SYN_SENT;
			
			//Wait for SYNACK
			//Then Send ACK
			int flag = recv_data(con, 0, 1); //hoping to get an SYNACK
			if(flag == SYNACK)
			{
				printf("Send ACK\n");
				// send an acknowledgement
				ci_packet_t *reply;
				reply = make_packet(con, "", HEADER_LEN, 0, 0, 0, ACK);
				sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);
				
				printf("Connection Established\n");
				con->state = STATE_ESTABLISHED;
			}
		}
		else if(con->state == STATE_LISTEN)
		{
			int flag = recv_data(con, 1, 0);
			if(flag == SYN)
			{
				printf("Got SYN\n");
				con->state = STATE_SYN_RCVD;
			}
		}
		else if(con->state == STATE_SYN_RCVD)
		{
			//printf("HELP!!!\n");
			int flag = recv_data(con, 0, 0);
			//printf("Flag: %d\n", flag);
			if(flag == ACK)
			{
				printf("Connection Established\n");
				con->state = STATE_ESTABLISHED;
			}
		}
		else if(con->state == STATE_ESTABLISHED)
		{
			pthread_mutex_lock(&(con->closing_lock));
			closing = con->closing;
			pthread_mutex_unlock(&(con->closing_lock));
			
			// get ready to send data, unless nothing else to do and quitting.
			pthread_mutex_lock(&(con->send_lock));
			// if we're closing the connection and there is nothing left to send, get out of here
			if(closing > 0 && con->send_buffer_len == 0) 
			{
				printf("Closing\n");
				
				if(closing == 1)
				{
					printf("Closing1\n");
					
					//send FIN here
					ci_packet_t *reply;
					reply = make_packet(con, "", HEADER_LEN, 0, 0, 0, FIN);
					sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, con->serv_info.ai_addr, con->serv_info.ai_addrlen);
					
					con->state = STATE_FIN_SENT;
				}
				else if(closing == 2)
				{
					printf("Closing2\n");
					
					//send FIN here
					ci_packet_t *reply;
					reply = make_packet(con, "", HEADER_LEN, 0, 0, 0, FIN);
					sendto(con->sfd, reply, reply->hdr.plen + C_PADDING, 0, (struct sockaddr *)&con->client_addr, con->client_addr_len);
					
					con->state = STATE_WAIT_FINAL_ACK;
				}
					
				continue; //until we've recieved a FIN and SENT an ACK
			}
			
			if(con->send_buffer_len > 0) 
			{
				// copy the data from the connection to the send buffer
				send_buffer = malloc(con->send_buffer_len);
				strcpy(send_buffer, con->send_buffer);
				send_len = con->send_buffer_len;
				// clear our send buffer
				free(con->send_buffer);
				con->send_buffer = NULL;
				con->send_buffer_len = 0;
				pthread_mutex_unlock(&(con->send_lock));

				if(window_peeper(con)){
					send_data(con, send_buffer, send_len);

				}
				else{
					waiting_queue++;
					strcpy(con->sendQueue[waiting_queue],send_buffer);

					printf("WARNING: no free space to send");
				}
				free(send_buffer);
			} 
			pthread_mutex_unlock(&(con->send_lock));
			
			int flag = recv_data(con, 0, 0);
			
			if(flag == FIN)
			{
				pthread_mutex_lock(&(con->closing_lock));
				con->closing = 2;
				pthread_mutex_unlock(&(con->closing_lock));
			}
			
			int stop_waiting = 0;
			pthread_mutex_lock(&(con->recv_lock));
			if(con->recv_buffer_len > 0) 
			{
				stop_waiting = 1;
			} 
			pthread_mutex_unlock(&(con->recv_lock));

			if(stop_waiting) 
			{
				pthread_cond_signal(&(con->recv_wait));
			}
			
		}
		else if(con->state == STATE_FIN_SENT)	
		{
			int flag = recv_data(con, 0, 0);
			
			if(flag == FIN)
			{
				printf("Got FIN1\n");
				con->state = STATE_WAIT_FINAL_ACK;
			}
			else if(flag == ACK)
			{
				con->state = STATE_WAIT_FINAL_FIN;
			}
		}
		else if(con->state == STATE_WAIT_FINAL_ACK)
		{
			int flag = recv_data(con, 0, 0);
			//printf("Flag %d\n", flag);
			
			if(flag == ACK)
			{
				printf("Got FIN1\n");
				con->state = STATE_FIN_DONE;
			}
		}
		else if(con->state == STATE_WAIT_FINAL_FIN)
		{
			int flag = recv_data(con, 0, 0);
			
			if(flag == FIN)
			{
				printf("Got FIN2\n");
				con->state = STATE_FIN_DONE;
			}
		}

		window_timer(con);
	}
	
    // close out.
    // ci_close should already have handled closing the socket and freeing info
    // will join the exited thread.
    pthread_exit(NULL);
    return NULL;
}